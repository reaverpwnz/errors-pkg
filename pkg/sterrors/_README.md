# Stack Trace Errors lib

## Описание
Пакет для стандартизации работы с ошибками. Он содержит интерфейс и типы ошибок, 
а также методы для удобства работы с ними. Основные фичи:
- Stacktrace
- Метод, инкапсулирующий оборачивание ошибок
- Стандартизация "архетипов" ошибок

## Как использовать
Возврат ошибок
```go
/* Раньше */
return nil, err

/* Сейчас */
return nil, sterrors.Wrap(err)
```

Создание кастомных ошибок. Если необходимый "архетип" не имплементирован, то 
можно добавить свой в errorTypes.go. (добавлять с умом, выбирать с умом)

```go
/* Раньше */
return nil, fmt.Errorf("user 228 not found")

/* Сейчас */
return nil, sterrors.NewNotFoundError("user 228 not found")
```

Получение стектрейса. (например, где-нибудь в логгере или еррорхендлере)
```go
err := errorIsHere() // тут ошибка с интерфейсом StackTraceError
stacktrace, anotherError := sterrors.GetStackTraceFromWrapped(err)
if anotherError != nil {
	fmt.Println(stacktrace)
}

/*
/Users/Kurisutina/Documents/work/errors_pkg/main.go:19
        main.errorIsHere
/Users/Kurisutina/Documents/work/errors_pkg/main.go:10
        main.main
/usr/local/go/src/runtime/proc.go:250
        runtime.main
/usr/local/go/src/runtime/asm_amd64.s:1594
        runtime.goexit
*/
```

Пример использования в хендлере.
```go
/* ... */

user, err := h.userUC.GetUserById(228)
if err == nil {
	return ctx.Status(http.StatusOK).JSON(user)
}
if errors.As(err, &sterrors.NotFoundError{}) {
	return ctx.SendStatus(fiber.StatusNotFound)
} else {
	return ctx.SendStatus(fiber.StatusInternalServerError)
}
```