package sterrors

import (
	"errors"
	"fmt"
	"runtime"
)

// Wrap оборачивает ошибку названием функции, из которой был произведен вызов Wrap.
// Должна использоваться на каждом этапе возврата ошибки (i.е. return err => return Wrap(err))
func Wrap(err error, prefix ...string) error {
	pc, _, _, ok := runtime.Caller(1)
	if !ok {
		return err
	}
	fn := runtime.FuncForPC(pc)

	pr := ""
	if len(prefix) != 0 {
		pr = prefix[0] + " "
	}
	return fmt.Errorf("%s%s: %w", pr, fn.Name(), err)
}

// GetStackTraceFromWrapped возвращает записанный стек первой попавшейся ошибки, имплементирующей StackTraceError.
// Создана для обработки обернутых ошибок на уровне логгера и т.п.
func GetStackTraceFromWrapped(err error) (string, error) {
	for ; err != nil; err = errors.Unwrap(err) {
		switch v := err.(type) {
		case StackTraceError:
			return v.Trace(), nil
		}
	}

	return "", fmt.Errorf("no wrapped StackTraceError found")
}
