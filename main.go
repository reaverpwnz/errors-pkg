package main

import (
	"errors"
	"errors_pkg/pkg/sterrors"
	"fmt"
)

func main() {
	err := errorIsHere()
	err = sterrors.Wrap(err, "customText")
	fmt.Println(err)
	fmt.Println(errors.As(err, &sterrors.NotFoundError{}))

	fmt.Println(sterrors.GetStackTraceFromWrapped(err))
	sterrors.NewNotFoundError("user 228 not found")
}

func errorIsHere() error {
	return sterrors.Wrap(sterrors.NewNotFoundError("govno found"))
}
