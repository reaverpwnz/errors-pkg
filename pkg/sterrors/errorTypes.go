package sterrors

/*

В этом файле должны быть имплементированы все "архетипы" используемых ошибок. Это необходимо для возможности проверки
ошибки на соответствие какому-то конкретному типу, чтобы была возможность отвечать клиенту (например) статус-кодом,
отличным от 500. (i.e. если errors.As(err, &NotFoundError{}) == true, то вернуть 404)

Контекст ошибок позволит как улучшить информативность ответов, так и даст возможность отслеживать ошибки каких-то
конкретных типов для мониторинга/метрик/пр. Простор для идей широкий.

PS для создания ошибки конкретного типа !!обязательно!! использовать конструкторы, через них инициализируются стектрейсы.

*/

type GenericError struct {
	errorText string
	stackTrace
}

func (e GenericError) Error() string {
	if e.errorText == "" {
		e.errorText = "ErrGeneric"
	}
	return e.errorText
}

func NewGenericError(customText ...string) StackTraceError {
	err := GenericError{}
	if len(customText) != 0 {
		err.errorText = customText[0]
	}

	err.stackTrace = newStackTrace()

	return err
}

type NotFoundError struct {
	errorText string
	stackTrace
}

func (e NotFoundError) Error() string {
	if e.errorText == "" {
		e.errorText = "ErrNotFound"
	}
	return e.errorText
}

func NewNotFoundError(customText ...string) StackTraceError {
	err := NotFoundError{}
	if len(customText) != 0 {
		err.errorText = customText[0]
	}

	err.stackTrace = newStackTrace()

	return err
}
